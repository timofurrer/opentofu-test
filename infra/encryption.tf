variable "passphrase" {
  sensitive   = true
  description = "Passphrase to ecnrypt and decrypt state and plan"
}

terraform {
  encryption {
    # method "unencrypted" "migrate" {}

    key_provider "pbkdf2" "this" {
      passphrase = var.passphrase
    }

    method "aes_gcm" "this" {
      keys = key_provider.pbkdf2.this
    }

    state {
      method = method.aes_gcm.this

      # fallback {
      #   method = method.unencrypted.migrate
      # }
    }

    plan {
      method = method.aes_gcm.this

      # fallback {
      #   method = method.unencrypted.migrate
      # }
    }
  }
}
