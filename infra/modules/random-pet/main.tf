terraform {
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "3.7.1"
    }
  }
}

resource "random_pet" "random_pet" {
  length = var.length
}
