output "project_name" {
  value = var.ci_project_name
}

output "environment" {
  value = var.environment
}
