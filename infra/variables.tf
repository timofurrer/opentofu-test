variable "ci_project_name" {
  type    = string
  default = "default"
}

variable "environment" {
  type = string
}
