module "random_pet" {
  source = "./modules/random-pet"
}

resource "local_file" "foo" {
  content  = "foo!"
  filename = "${path.module}/foo.bar"
}
